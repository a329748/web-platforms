const log4js = require("log4js")

let logger = log4js.getLogger()

logger.level = "debug"

logger.info("App started successfully.")
logger.warn("Missing dependencies.")
logger.error("Missing required files.")
logger.fatal("App failed to initialize.")

function sum(x, y) {
	return x + y
}

module.exports = sum