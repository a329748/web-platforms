const sum = require("../index")
const assert = require("assert")

describe("Test the sum of two numbers", () => {
	it("2 plus 2 equals 4", () => {
		assert.equal(4, sum(2, 2))
	})
	it("2 plus 2 does not equal 22", () => {
		assert.notEqual("22", sum(2, 2))
	})
})
